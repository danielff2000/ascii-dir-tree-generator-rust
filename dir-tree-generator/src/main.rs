use std::fs::{self, DirEntry};
use std::io;
use std::path::Path;

fn visit_dirs(dir: &Path, prefix: String) -> io::Result<()> {
    if dir.is_dir() {
        let entries = fs::read_dir(dir)?
            .filter_map(Result::ok)
            .collect::<Vec<DirEntry>>();
        
        for (i, entry) in entries.iter().enumerate() {
            let file_name = entry.file_name().into_string().unwrap_or_else(|_| "?".to_owned());
            let connector = if i == entries.len() - 1 { "└── " } else { "├── " };

            println!("{}{}{}", prefix, connector, file_name);

            if entry.path().is_dir() {
                let new_prefix = if i == entries.len() - 1 {
                    format!("{}    ", prefix)
                } else {
                    format!("{}│   ", prefix)
                };
                visit_dirs(&entry.path(), new_prefix)?;
            }
        }
    }
    Ok(())
}

fn main() -> io::Result<()> {
    let root = Path::new(".");
    visit_dirs(&root, "".to_owned())?;
    Ok(())
}
